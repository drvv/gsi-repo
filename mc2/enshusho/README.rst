NOTE ON IMAGES  (23.03.2013)
==============================

Location of the images
----------------------------

In this project, all images are located at './images', where '.' represents the dierctory where 'master.tex' is located.


How to specify in TeX sources
-------------------------------

Since when you run 'platex' in the terminal the compiler recognises all the relative references according to 'master.tex', where *the command is runned*.

Thus in the tex source files, with the "special rule" for this project, I write 

  ./images/image-to-display.eps

