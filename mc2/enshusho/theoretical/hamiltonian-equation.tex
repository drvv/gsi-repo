
\section{ルジャンドル変換}
\label{sec:legendre-transformation}

ルジャンドル変換は、熱力学や解析力学の文脈では、独立変数の入れ替え操作のことを指しています。
言葉で説明するよりも、穴埋め形式で追ってみましょう。
\begin{majorsh}{ルジャンドル変換}
  \mbox{}

  \begin{exercise}
    以下の空欄を埋めよ。

  一変数関数$f(x)$に対して、次のような関数$f^\star$を定める。
  \begin{align}
    f^\star \equiv x y - f(x).
    \label{eq:legendre-trans}
  \end{align}
  ここで、$y$とは
  \begin{align}
    y \equiv \pardiff{f}{x}.
    \label{eq:def-of-canonical-variable}
  \end{align}
  と置いたもので、$x$とは\uline{独立と考える}。 
  すると見た目からは、$f^\star$が依存している独立変数は、\fbox{\phantom{$x,y$の二変数}}に思える。すなわち
  \begin{align*}
    f^\star = f^\star (\pbox{x},\pbox{y})
  \end{align*}
  と書けるように思える。

  ここで、$f^\star$の全微分を調べてみよう。すると
  \begin{align*}
    df^\star= \pardiff{f^\star}{\pbox{x}} d\,\pbox{x} + \pardiff{f^\star}{\pbox{y}} d\,\pbox{y}
  \end{align*}
  となるが、実際に$f^\star$を代入して計算してみると
  \begin{align*}
      \pardiff{f^\star}{x} = \pbox{0}.
  \end{align*}  であることがわかる。つまり、\eqref{eq:legendre-trans},\eqref{eq:def-of-canonical-variable}によって定義された関数$f^\star$は、\uline{$x$に陽に依存していないことがわかった。}すなわち
  \begin{align*}
    f^\star = f^\star (\pbox{y})
  \end{align*}
  と書くべきである。

このようにして独立変数を入れ替える操作のことを、\tech{ルジャンドル変換}と呼ぶ。
\end{exercise}
\begin{exercise}
  以上の議論を$n$変数関数$f(x_1,\dots,x_n)$の場合に一般化し、そのルジャンドル変換$f^\star$が
  \begin{align*}
    f^\star &= \sum_{i=1}^n x_i y_i - f \ , \\
    y_i &= \pardiff{f}{x_i}.
  \end{align*}
  で与えられることを確認せよ。
\end{exercise}
\end{majorsh}

ざっくりまとめると、
\begin{align}
  f(\mbox{新変数}) = \mbox{旧変数}\times\mbox{新変数} - f(旧変数)
\end{align}
といった具合です。
% \begin{majorsh}{多変数関数のルジャンドル変換}
%   \mbox{}

%   $n$変数関数$f(x_1,\dots,x_n)$のルジャンドル変換$f^\star$は
% \end{majorsh}

\section{ハミルトニアン}
\label{sec:hamiltonian}

\tech{ハミルトニアン}$H$は、ラグランジアン$L$の一般化速度$\dot{q}$をすべて一般化運動量$p$にルジャンドル変換した量として定義されます。
\begin{align}
  H(q,p) \equiv p\dot{q} - L(q,\dot{q}).
\end{align}
多自由度の場合は、多変数関数のルジャンドル変換をするだけです。
ハミルトニアンには単にラグランジアンのルジャンドル変換という形式的な意
味だけでなく、物理的な意味も存在します。具体例を通して見てみましょう。

\begin{majorsh}{ハミルトニアンの具体計算}
  \begin{exercise}
    ラグランジアン$L$が次のように与えられているとき、ハミルトニアン$H$を求めよ。
    \begin{minor}
    \item $L(x,\dot{x})=\half m \dot{x}^2 - U(x)$
    \item $L(r,\theta,\dot{r},\dot{\theta}) = \half m (r^2 +
      \sq{r\dot{\theta}}) - U(r,\theta)$
    \item $L(r,\theta,\phi)=\half m (r^2 + \sq{r\dot{\theta}} + \sq{r\dot{\phi}\sin\theta})$
    \end{minor}
  \end{exercise}
  \begin{exercise}
    次の空欄を埋めよ。
    \begin{center}
      ハミルトニアンは、上の具体例からも分かるように系の\pbox{運動エネル
        ギー}と\pbox{位置エネルギー}の和になっていることから、系の
      \pbox{全エネルギー}を与える関数である。\footnote{一般の場合は、
        演習問題を参照してください。}
    \end{center}
  \end{exercise}
\end{majorsh}

\section{ハミルトンの方程式}
\label{sec:canonical-equation}

\tech{ハミルトンの方程式}\footnote{正準\english{canonical}方程式とも呼ばれること
  があります。}は、ハミルトニアン$H$を用いた運動方程式のことを指します。穴
埋め形式で確認しましょう。

\begin{majorsh}{ハミルトンの方程式の導出}
  \mbox{}

  次の空欄を埋めよ。
  
  ハミルトニアン$H$の全微分を考えてみる。ハミルトニアンの定義に従って
  \begin{equation}
    \label{eq:total-derivatives-of-H}
    dH = d(p\dot{q}) - dL.
  \end{equation}
    ここでラグランジアン$L$は$q,\dot{q}$の関数だから
    \begin{equation*}
      dL = \pardiff{L}{q} dq + \pardiff{L}{\dot{q}} d\dot{q}.
    \end{equation*}
    また、全微分のライプニッツ則から
    \begin{equation*}
      d(p\dot{q}) = q\, dp + pd\dot{q}.
    \end{equation*}
    さらに
    \begin{equation}
      \label{eq:canonical-momentum}
      p=\pardiff{L}{\dot{q}}
    \end{equation}
    に注意して \eqref{eq:total-derivative-of-H}を計算すると
    \begin{align*}
      \label{eq:1}
      dH&= \dot{q} dp - \pardiff{L}{q} dq.\\
      \intertext{ここで、ラグランジュの運動方程式より}
      dH&= \dot{q} dp - \diff{}{t} \pardiff{L}{\dot{q}}.
    \end{align*}
    再び\eqref{eq:canonical-momentum}に注意すると、
    \begin{align*}
 %     \left \{
%        \begin{matrix}
          \pardiff{H}{p} &= \diff{q}{t}, \\
          \pardiff{H}{q} &= -\diff{p}{t}.
%        \end{matrix}
%      \right.
    \end{align*}
    という連立\uline{一階}微分方程式が得られる。この方程式をハミルトンの方程式
    と呼ぶ。
\end{majorsh}

\section{ハミルトン形式による解法の具体例}
\label{sec:hamiltonian-mechanics-eg}
ラグランジュの方程式は、一般化座標$q$についての二階微分方程式でした。いまハミルトンの方程式は、一般化座標$q$と一般化運動量$p$の独立な二つの変数の一階微分方程式なので、運動の得られ方が少し異なります。ラグランジュ形式の場合はニュートン力学と同様、座標$q$が時間の関数として求まり、それから運動量$p$が求まるという順序がありましたが、ハミルトン形式の場合は連立方程式なので座標$q$と運動量$p$が同時に求まります。

また、$x,p$を独立として扱っていることから、質点の運動を$x\text{--}p$平面の中の運動として捉えることができます。この$x\text{--}p$平面のことを相空間\english{phase space}と呼びます。\footnote{しばしば、phase を位相と訳して「位相空間」と呼ばれることがあります。しかしこれは数学の文脈では topological space という概念を指しますので、注意が必要です。}ただちにわかるように、$n$自由度の場合、相空間の次元は$2n$次元になるので、図示することは困難になります。

具体的にハミルトンの方程式を解くという作業をやってこの章を終えることにします。

\begin{majorsh}{具体例--調和振動子}
  一次元調和振動子型ポテンシャル$V(x)=\half m\omega^2x^2$中を運動する質量$m$の質点について、以下の問いに答えよ。
  \begin{exercise}
    ラグランジアン$L$を書き、$x$に共軛な運動量$p$を求めよ。
  \end{exercise}
  \begin{exercise}
    ハミルトニアンを$L$のルジャンドル変換として求めよ。
  \end{exercise}
  \begin{exercise}
    ハミルトンの方程式を立てて、初期条件$x(0)=0, p(0)=p_0 (>0)$のもとで解け。
  \end{exercise}

  一般に、質点の運動は$x\text{--}p$平面上の軌跡として捉えることができる。
  \begin{exercise}
    前問で求めた解$x(t),p(t)$について、$x\text{--}p$平面上の軌跡を図示せよ。
  \end{exercise}
\end{majorsh}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "theory-master"
%%% End: 
