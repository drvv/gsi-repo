%% chapter is already sectioned in 'theory-master'

\begin{summary}
この章では、解析力学にとって重要な概念である座標・座標系の基本をおさらいし、一般化座標の考え方を理解します。
\end{summary}

\section{座標・座標系・座標変換}
\label{sec:coordinates-coordinateSystem-coordinateTransformation}

質点の\techterm{座標}とは、質点の位置を表す\desc{数(の組)}のことを言います。\techterm{座標系}とは、\desc{座標の与え方}のことを指します。ざっくり言うと、位置の「計り方」のことです。

したがって、質点の位置を表す方法は、座標系の数だけあります。座標系が異なれば、同じ点に与える座標、すなわち数の組が異なります。\techterm{座標変換}とは、座標系を変えることによって、点に与える座標を変えることを言います。

%%　固定、でも回転した座標系や、並進しただけの座標系の説明を入れるかどうか・・・。
%% 図をつけたり、具体値をだしたりして、具体的にする方が良いだろうか。

\subsection*{代表的な座標系}
\label{sec:typical-coordinateSystem}
簡単なおさらいは以上です。それでは代表的な座標系を復習しましょう。

\begin{majorsh}{二次元極座標}
  \begin{exercise}
    点Pの位置はデカルト座標で$(\sqrt{3},1)$と表されている。このとき、OPの長さ$r$とOPが$x$軸と成す角$\theta(0\leq \theta < 2\pi)$を求めよ。なお、このようにして決めた数の組$(r,\theta)$を、Pの\techterm{(二次元)極座標}という。
  \end{exercise}
  \begin{exercise}
  一般に点Qが二次元極座標で$(r,\theta)$と表されている。このとき、点Qのデカルト座標$(x,y)$を$r,\theta$を用いて表せ。
  \end{exercise}
\end{majorsh}
\begin{sols}
  \begin{sol}
    三平方の定理から
    \begin{align*}
      r&=\sqrt{\sq{\sqrt{3}}+\sq{1}} = 2,\\
      \cos\theta &= \frac{\sqrt{3}}{2}, \quad \sin\theta=\frac{1}{2}. \therefore \theta = \frac{\pi}{6}.
    \end{align*}
  \end{sol}
  \begin{sol}
    図より
    \begin{align*}
      \left\{ 
      \begin{matrix}
        x=r\cos\theta \\
        y=r\sin\theta.
      \end{matrix}
      \right.
    \end{align*}
  \end{sol}
  \marginpar{
%    \begin{figure}[h]%{r}{0.2\paperwidth}
      \centering
      \includegraphics[width=0.8\marginparwidth]{./images/dimTwo-polar}
      \captionof{figure}{二次元極座標}
      \label{fig:two-dim-polarCoordinates}
  %  \end{figure}
  }

\end{sols}

\begin{majorsh}{三次元極座標}
  \begin{exercise}
    点Pの三次元極座標を$(r,\theta,\phi)$とする。このとき、$\theta,\phi$がどこの角を表しているかを明示して、点Pとxyz軸を図示せよ。
  \end{exercise}
  \begin{exercise}
    三次元極座標とデカルト座標の座標変換を与える式を求めよ。
  \end{exercise}
\end{majorsh}

\begin{majorsh}{円筒座標}
  \begin{exercise}
     点Pの円筒座標を$(\rho,\theta,z)$とする。このとき、$\theta$がどこの角を表しているかを明示して、点Pとxyz軸を図示せよ。
  \end{exercise}
  \begin{exercise}
    円筒座標とデカルト座標の座標変換を与える式を求めよ。
  \end{exercise}
\end{majorsh}

\subsection*{一般化座標}
\label{sec:generalizedCoordinates}

\tech{一般化座標}とは、質点の位置を完全に決定するような数の組全般のことを指します。\footnote{前述のように「座標」という言葉を用いるなら、あえて「一般化」と呼ばなくても良いわけですが、代表的な座標系に属さないものも含めるためにあえて「一般化」と名付けられています。}

たとえば、ターザンロープにのった人の位置は、どのように表すのが都合が良いでしょうか。
\marginpar{
  \centering
  \includegraphics[width=\marginparwidth]{./images/tasan}
  \captionof{figure}{ターザンロープ}
  \label{fig:tasan}
}
この系を単純化すると左図のようになるでしょう。(図\ref{fig:tasan})ここで$l$は原点から計った、ロープの付け根の位置、$R$はロープの長さ、$\theta$はロープが鉛直線と成す角を表しています。ロープの長さは変わらないとすると、質点の位置は、ロープの付け根が、レール上のどこにいるか、そしてロープはどのくらい傾いているかで完全に決定でき、これがこの質点の位置を記述するのに自然なものでしょう。つまり、このようにして数の組$(l,\theta)$を定めることにすれば、$(l,\theta)$は質点の\uline{座標である}といえます。

しかし、これは先ほど見たような座標系による座標ではないことは明らかです。このように同じ系の質点に対する座標の与え方は無数にあり、それらを一括りに\tech{一般化座標}と読んでいるわけです。解析力学において一般化座標が強調される理由は、次第に明らかになるでしょう。

\begin{majorsh}{``ターザン''座標}
\mbox{}

図\ref{fig:tasan}に置いて、鉛直上向きおよびロープに沿って右向きを正とするとき、座標$(l,\theta)$とデカルト座標との座標変換の式を求めよ。
\end{majorsh}

% \subsection*{一般化速度}
% \label{sec:generalized-velocity}

なお、\tech{一般化速度}という言葉がしばしば用いられることがありますが、
これは一般化座標$q$の時間微分
$\dot{q}\equiv\diff{q}{t}$のことを指します。ただしこの名称は形式的であ
ることに注意しましょう。すなわち具体的には、二次元極座標$(r,\theta)$
において、これを一般化座標と思ったとき、それぞれの一般化速度は
$\dot{r},\dot{\theta}$です。しかし、たとえば角度方向の実際の物理的な速
度は、$r\dot{\theta}$です。このように、一般化速度は速度の次元を持って
いないことがしばしばありますが、名前の付け方だけの問題と考えてよいでしょ
う。

\section{自由度}
\label{sec:degree-of-freedom}

一つの質点の位置は、この空間の次元が三次元と考えれば$3$つのデカルト座標を用い
て現わされます。しかし状況によっては、質点の位置を一意的に決定するのに必要かつ十分な座標の個数は、
$3$とは限りません。

たとえば、ビリヤードの球の運動を考えるにあたっては、
ビリヤード台という平面のどこにあるかだけを考えれば十分です。すると、実
際には$2$つの数を使って位置を表すことができます。
\footnote{もちろん、球は跳ねる事があります!　ここではそういう状況を考えない、ということ
に過ぎません。}

また、ジェットコースターの位置は確かに時々刻々、三次元空間内を移動します
が、スタート地点からどれくらいの距離に居るかという情報一つのみで、ジェッ
トコースターの位置は決定できます。したがってここでは
$1$つの座標があれば十分です。
\footnote{もちろん、ジェットコースターとて跳ねたり落ちたりする事はあるでしょう!　しかしここでも、そういう状況は考
えない、言い換えれば「状況設定による」ということです。}

このように、位置ベクトルを決定するのに必要でかつ十分な座標の数の事を\tech{自
  由度}といいます。また、自由度を減らすような条件のことをを\tech{拘束
  条件}といいます。

$n$個の質点の位置は$3n$個
% \tech{自由度}とは、質点や剛体の位置を決定するために必要でかつ十分な座
% 標の個数のことをいいます。

\section{一般化座標を用いた速さの表示}
\label{sec:componentsOfVectors}

% 原点を始点、質点の位置を終点とするようなベクトルを、\tech{位置ベクトル}といいました。また、\tech{速度ベクトル}は位置ベクトルの\tech{時間微分}で得られました。

% ベクトルの\tech{成分}は、\tech{基底ベクトル}を決めることで決まる数(の組)のことでした。速度を

解析力学的な手法で力学の問題をとくために最も重要になることは、系の運動エネルギーを求めることなのですが、そのためには一般化座標を用いて速さ(の二乗)を表すことが必要です。

速さは微小変位を微小時間で割ったもの
\begin{align*}
  v^2 = \sqmodnp{\frac{d\bm{r}}{dt}} = \frac{\sqmodnp{d\bm{r}}}{\sq{dt}}.
\end{align*}
と考えられますから、質点の微小変位を一般化座標で表すことができればよいわけです。
\begin{majorsh}{速さの表示}
\mbox{}

  次の座標を用いて、速さの二乗$v^2$を表示せよ。
  \begin{exercise}
    二次元極座標
  \end{exercise}
  \begin{exercise}
    三次元極座標
  \end{exercise}
  \begin{exercise}
    円筒座標
  \end{exercise}
  \begin{exercise}
    ``ターザン''座標
  \end{exercise}

\end{majorsh}

%逆に、数の組としては異なるけれども、同じ位置を表すことがあります。

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "theory-master"
%%% End: 
