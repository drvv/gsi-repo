今回は解析力学の理論の中でも最重要な、変分法とオイラー・ラグランジュ方程式の導出を行います。この章の内容は変分法、オイラー・ラグランジュ方程式をあえて物理学的な内容とは切り離した「道具」として導入します。（たとえば微分法は、数学として勉強するが、物理学としては勉強しないことと同じです。）こうすることによって、「解析力学がよくわからない」というざっくりとした症状は「解析力学で使われる数学（式変形）がわからない」というより具体的な課題として捉えることができるようになります。% たぶんこれは前文とかに入れた方が良いのだろう…

\section{変分法}
\label{sec:variational-method}

\subsection*{汎関数}
\label{sec:functional}

%変分法とは、汎関数の極値を求める方法のことを言います。%
一般に関数は、何かを数に対応させる規則でした。この章で扱う汎関数とは、関数を数に対応させる規則のことを指します。

解析力学で扱う汎関数は、積分です。こういうものは具体例を見るに限ります。たとえば次のような定積分は汎関数です。
\begin{align*}
  I[f]  = \int_0^\pi f(x) dx.
\end{align*}
定積分値は、関数$f$を定めることで数が得られます。たとえば$I[x^2]=\frac{\pi^3}{3}, I[\sin x]= 2$と言った具合です。

もう少し具体的な例を挙げましょう。
\begin{majorsh}{曲線長}
  \begin{exercise}
    質点の軌道が$y=f(x)$と表されているとする。$x=0$から、$x=1$までに、質点が進む距離$s$の表式を求めよ。
  \end{exercise}
  \begin{exercise}
    上で求めた$s$(曲線長ともいう)が汎関数であることを説明せよ。
  \end{exercise}
\end{majorsh}

\subsection*{汎関数の変分}
\label{sec:calculus-of-variation}

\tech{変分法}とは、\desc{汎関数の極値を求める方法}のことを言います。

微分と変分は、形式的にはとても似ていますので、対比しながら変分法の基本を見ていきます。

微分法は、関数$f(x)$の引数$x$を少しずらしたときに、どれだけ値が変化するかを調べる方法でした。変分法では、汎関数の引数である関数を少しずらしたときに、どれだけ値が変化するかを調べます。ここで、関数を少しずらす、とは
\begin{align*}
  f(x) \longrightarrow f(x) + \delta f(x)
\end{align*}
という操作をすることです。ここで$\delta f(x)$というのは、$x$に依存して加える微少量のことで、微分法のときと同様
\footnote{微分法では、$x \longrightarrow x+d x$としましたが、 このとき$x$と$dx$は独立な量でした。}、$f(x)$とは独立な量($f(x)$の値とは関係なく決められる量)です。イメージ図を書くと左の図のようになります。
\marginpar{
\captionof{figure}{関数の微小変化}  
}

関数$f$の微分$df$は、次のような量のことでした。
\begin{align*}
  df = f(x+dx) - f(x) = \pardiff{f}{x} dx.
\end{align*}

汎関数$s[f(x)]$の変分とは、次のような量のことを指します。
\begin{align*}
  \delta s[f(x)] = s[f(x)+\delta f(x)] - s[f(x)].
\end{align*}
関数の微分と形が非常に似ていることに気づくでしょう。

関数$f$が極値をとるということは、微分$df$が$0$となること、すなわち
\begin{align*}
  df = f(x+dx)-f(x) = 0
\end{align*}
が、\emph{$dx$によらずに成立すること}でした。

同様に、汎関数$s$が極値をとるということは、変分$\delta s$が$0$となること、すなわち
\begin{align*}
  \delta s = s[f(x) +\delta f(x)] - s[f(x)] = 0
\end{align*}
が、\emph{$\delta f(x)$によらずに成立すること}です。
% $s$が定積分で表されている場合は、
% \begin{align*}
%   \delta s[f(x)] &= \int_0^1 f(x)+\delta f(x) dx - \int_0^1 f(x) dx \\
%   &= \int_0^1 \delta f(x) dx = s[\delta f(x)].
% \end{align*}
% のようになります。

変分法の心をつかむために、具体例を見ましょう。代表的な汎関数として、上で曲線長を求めました。さて、二次元Euclid平面上の二点を結ぶ最短経路は、直線であることは知っています。これを変分法を用いて証明しましょう。この問題には、後にやるオイラー・ラグランジュ方程式の導出に用いられる論理が全て詰まっています。

\begin{majorsh}{最短距離を与える曲線}
  \mbox{}

  二次元Euclid平面上の曲線を$y=f(x)$とする。いま、原点O$(0,0)$と点P$(1,1)$を結ぶ最短経路を以下の手順に従って求めよう。
  \begin{exercise}
    曲線長$s$の変分$\delta s$を計算せよ。
  \end{exercise}
  \begin{exercise}
    曲線長の変分$\delta s$が極値をとるとき、$\delta s$の値を求めよ。
  \end{exercise}
  \begin{exercise}
    $\delta s$の計算結果について、部分積分を実行することで
    \begin{align*}
      \delta s = -\int_0^1 dx \diff{g(x)}{x} \delta f(x)
    \end{align*}
    の形に整理せよ。ただし$\delta f(0) =0$かつ$\delta f(1)=0$であることに注意し、なぜこのような条件を用いるのかも考えよ。
  \end{exercise}
  \begin{exercise}
    $\delta s$が極値をとるための条件から、$f'(x)$が満たす微分方程式を求めよ。ただし、$\delta f(x)$は任意の微少量であることを用いよ。
  \end{exercise}
  \begin{exercise}
    $f'(x)$について求めた条件式から、$f(x)$を求めよ。
  \end{exercise}

\end{majorsh}

\section{オイラー・ラグランジュ方程式}
\label{sec:euler-lagrange-eq}

上でやった問題に、オイラー・ラグランジュ方程式を導出するエッセンスは全て含まれています。あとは上の議論をもう少し一般化するだけです。問題の形式でやってみましょう。

\begin{majorsh}{オイラー・ラグランジュ方程式の導出}
  \mbox{}

  汎関数$S$が次のように与えられているとする。
  \begin{align*}
    S[q(t)] = \int_a^b dt L(q(t),\diff{q}{t}(t)).
  \end{align*}
  ただし$L$は$q$と$q$の$t$微分$\diff{q}{t}\equiv \dot{q}$の関数である。この汎関数が極値をとるとき、すなわち変分$\delta S$が$0$になるとき、$L$が満たす方程式(オイラー・ラグランジュ方程式)を求めよう。

  \begin{exercise}
    以下の空欄にを埋め、オイラー・ラグランジュ方程式の導出を完成せよ。

  \begin{align}
    \label{eq:diff-of-action}
     \delta S &=
     \int_a^b dt \  L(q(t)+\delta q(t), \diff{}{t} \pbox{$\paren{q(t)+\delta q(t)}$}) \\ &\quad - \int_a^b dt L(q(t),\dot{q}(t)). \nonumber\\
     \intertext{ここで、}
    \diff{}{t} {\delta q(t)} &= 
    \frac{\delta q(t+dt) - \delta q(t)}{dt}
    =\delta \paren{\frac{q(t+dt)-q(t)}{dt}}
    =\delta \dot{q}(t) \label{eq:diff-in-variation} \\
    \intertext{に注意し、\eqref{eq:diff-of-action}において積分範囲が等しいので、}
    \delta S &= \int_a^b dt \curly{L(q+\delta q,\dot{q}+\delta \dot{q}) - L(q,\dot{q})}.\nonumber% % \\
     \intertext{$\delta q, \ \delta \dot{q}$はともに微少なので、一次近似は}
     \delta S &= \int_a^b dt \paren{ \ \bphantom{\pardiff{L}{q}}\  \delta q + \ \bphantom{\pardiff{L}{\dot{q}}}\  \delta \dot{q} }.\nonumber\\
    \intertext{再び\eqref{eq:diff-in-variation}を用いると}
    &=\int_a^b dt \paren{\ \bphantom{\pardiff{L}{q}}\  \delta q + \bphantom{\pardiff{L}{\dot{q}}}\  \diff{}{t} \paren{\delta q} }. \nonumber\\
    \intertext{ここで、変分の端点を固定する、すなわち}
   &\qquad\delta q(a) = \delta q(b) = \bphantom{0} \nonumber\\
    \intertext{として二項目を部分積分して$\delta q$で括ると}
    \delta S &=\int_a^b dt \ \paren{\ \bphantom{\pardiff{L}{q} - \diff{}{t}\pardiff{L}{\dot{q}}}\ }\ \delta q. \nonumber
  \end{align}
    $S$が極値をとるとき、任意の変分$\delta q$について
  \begin{align*}  
    \delta S &= \bphantom{0}
  \end{align*}
  が成立するので、$\delta q$の係数は
  \begin{equation}
    \label{eq:euler-lagrange}
    \bphantom{\pardiff{L}{q} - \diff{}{t}\pardiff{L}{\dot{q}}} = 0. 
  \end{equation}
  を満たさなければならない。
  
    この\eqref{eq:euler-lagrange}を、オイラー・ラグランジュ方程式と呼ぶ。
  \end{exercise}
  \begin{exercise}
  「最短距離を与える曲線」で求めた曲線の方程式を、今度は
  オイラー・ラグランジュ方程式を用いることで再び導出せよ。
  \end{exercise}

\end{majorsh}

  上の議論は一般的なものなので、$S$を$s$、$L$を線素として適用すれば、
  最短距離を求めることができるはずです。

今までの議論では一自由度(自由な変数の個数が一つ)の場合のみをやってきま
したが、オイラー・ラグランジュの方程式の強みの一つとして、自由度がいく
ら増えても全く同じ形をしているというのがあります。これを確かめましょう。
\begin{majorsh}{多自由度の場合のオイラー・ラグランジュ方程式}
  \mbox{}

  汎関数$S$は次のように与えられているとする。
  \begin{equation*}
    S[q_1,\dots,q_n]= \int_a^b dt \,
    L(q_1,\dots,q_n,\dot{q}_1,\dots,\dot{q}_n).
  \end{equation*}
  ただし$q_j(j=1,\dots,n)$は$t$の関数である。$S$が極値をとるとき、$j=1,\dots,n$に
  ついて
  \begin{equation*}
    \pardiff{L}{q_j} - \diff{}{t}\pardiff{L}{\dot{q}_j} = 0
  \end{equation*}
  が成立することを示せ。
\end{majorsh}
% \subsection{作用}
% \label{sec:action}

% 作用($S$と書かれます)とは、次のような汎関数です。
% \begin{align*}
%   S[q(t)] = \int_a^b dt L(q,\dot{q},t) .
% \end{align*}
% ここで$q(t)$は質点の一般化座標です。デカルト座標などであれば$x(t)$や$\theta(t)$と書かれるところですが、どの座標系を用いているかを問題にしていないので、一般的に$q(t)$のように書かれます。簡単のために一自由度の場合に書いてありますが、自由度が高くなっても全く同様です。

% $L(q,\dot{q},t$は、系のラグランジアンと呼ばれる量です。具体的には運動エネルギー$T$と位置エネルギー$U$の差
% \begin{align*}
%   L=T-U
% \end{align*}
% で与えられる量ですが、オイラー・ラグランジュ方程式を導くにあたっては$L$の中身は気にする必要はありません。とにかく$q,\dot{q}$を含んでいる関数だと思っていてください。

% \subsection{最小作用の原理}
% \label{sec:principle-of-least-action}




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "theory-master"
%%% End: 
