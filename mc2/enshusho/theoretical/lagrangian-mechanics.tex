
\section{最小作用の原理}
\label{sec:principle-of-least-action}

最小作用の原理とは、「質点の軌跡は、作用という量が最小になるように決まる」ことを主張する、解析力学の理論の根本をなす原理です。この章ではNewtonの運動方程式を導き、さらに解析力学の手法のうまみを感じることを目標にします。

\subsection{作用とラグランジアン}
\label{sec:action}

作用$S$とは、次のような、座標$q(t)$の汎関数です。
\begin{equation}
  S[q(t)] = \int_a^b dt L(q,\dot{q},t) .
  \label{eq:action}
\end{equation}
ここで$q(t)$は質点の一般化座標です。デカルト座標などであれば$x(t)$や$\theta(t)$と書かれるところですが、どの座標系を用いているかを問題にしていないので、一般的に$q(t)$のように書かれます。簡単のために一自由度の場合に書いてありますが、自由度が高くなっても全く同様です。

$L(q,\dot{q},t)$は、系のラグランジアンと呼ばれる量です。具体的には運動エネルギー$T$と位置エネルギー$U$の差
\begin{align*}
  L=T-U
\end{align*}
で与えられる量です。% この形は、d'Alembertの原理と仮想仕事の原理を仮定すれば導くことが出来ます(演習問題を参照)が、
以下の議論ではではひとまず、天下り的に与えられたものとします。

\subsection{ラグランジュの運動方程式}
\label{sec:lagrangian-eqofmot}

作用$S$は汎関数ですから、$S$が最小値をとるための条件\footnote{もちろん、$\delta S =0$だけでは、これは$S$が極値をとるための条件であって、極小値や最小値をとるための十分条件ではありません。しかし、実際はラグランジアンの凸性によって、これで十分であることが確かめられます。}は変分法を用いて
\begin{align*}
  \delta S = 0
\end{align*}
のように書けました。これが成り立つ時、ラグランジアン$L$が満たすべき条件は、Euler-Lagrange方程式と呼ばれる式でした。
\begin{equation}
  \label{eq:lagrange-equation}
  \diff{}{t}\pardiff{L}{\dot{q}} = \pardiff{L}{q}.
\end{equation}

前の章で導いた際は、物理学的な背景は全くありませんでしたが、今度の$L$はラグランジアンという、質点の座標$q$とその速度$\dot{q}$の関数です。したがって、\eqref{eq:lagrange-equation}は、質点の座標と速度が満たすべき関係式ということになります。この意味で\eqref{eq:lagrange-equation}は、ラグランジュの運動方程式と呼ばれます。

\subsection{Newtonの運動方程式の導出}
\label{sec:derivation-of-newtonian-eqofmot}

最小作用の原理を仮定すると、質点の座標$q$と速度$\dot{q}$はラグランジュの運動方程式\eqref{eq:lagrange-equation}を満たしていなければならないことが言えました。具体例を通して、\eqref{eq:lagrange-equation}はニュートンの運動方程式に他ならないことをみましょう。

\begin{majorsh}{具体計算}
  \begin{exercise}
    質量が$m$の質点が、以下の条件にある場合のラグランジアン$L$を求めよ。ただし、それぞれ括弧内の座標を用いること。
    \begin{minor}
    \item 原点が振動中心でバネ定数$k$の一次元調和振動子(一次元デカルト座標)
    \item 重力加速度$g$の一様重力場(二次元デカルト座標)
    \item 万有引力定数$G$、重力源の質量$M$のケプラー問題(二次元極座標、重力源は静止しているとし、その位置を原点とせよ)
    \end{minor}
  \end{exercise}
  \begin{exercise}
    上の全ての場合について、ラグランジュの運動方程式を具体的に計算し、ニュートンの運動方程式と一致することを確認せよ。
  \end{exercise}
\end{majorsh}


\section{座標変換}
\label{sec:coordinate-transformation-in-lagrange}

異なる座標系を用いたときに、運動方程式はどのように変わるかという問題は、物理学で重要な問題です。\ref{cha:coordinates}章の演習問題では、具体的に座標変換などをして極座標等の運動方程式を求めましたが、解析力学の方法では明快に、他の座標系を採用したときの運動方程式の表式を求めることが出来ます。さっそく例を見ていきましょう。

\begin{majorsh}{極座標の運動方程式}
\mbox{}

  質量$m$の質点が、二次元平面上をポテンシャル$U$の中で運動している。
  \begin{exercise}
    ラグランジアン$L$を$r,\theta,\dot{r},\dot{\theta}$の関数として表せ。(運動エネルギーを二次元極座標で表示せよ)
  \end{exercise}
  \begin{exercise}
    $r,\theta$それぞれについてラグランジュの運動方程式を書け。
  \end{exercise}
  \begin{exercise}
    ラグランジュの運動方程式に、上で求めた$L$を代入し、その結果、二次元極座標系で見たニュートンの運動方程式が得られていることを確認せよ。
  \end{exercise}
  \end{majorsh}

以上のように、座標系を変更するために生ずる手間は、一番最初の\uline{ラグランジアンの表示を決定することだけ}に凝縮されていることが判ったと思います。この簡潔さは、ラグランジュの運動方程式が、各座標について\emph{すべて同じ形をしている}ことから来ていることも感じられたでしょう。

また、ニュートンの力学は慣性座標系を基準に作られていたために、座標系が
慣性系か否か、後者であれば慣性力という力を考慮する必要がありましたが、
上でやったような解析力学の手法では、何も考えること無く、慣性力の項も
\emph{自然に}現れています。このように形式的に物理学の議論が出来ること
も、解析力学の魅力の一つといえるでしょう。

\section{一般化運動量}
\label{sec:generalized-momentum}

一般化座標$q_i$に対する一般化運動量$p_i$とは、ラグランジアン$L$を一般化速度$\dot{q}_i$で微分した量のことを言います。
\begin{equation*}
  p_i \equiv \pardiff{L}{\dot{q}_i}.
\end{equation*}
この一般化運動量$p_i$は、$q_i$の\tech{共軛運動量}、\tech{正準共軛な
  運動量}という呼ばれ方もします。

定義だけでは何のことかよくわからないので、具体例を確認しましょう。
\begin{majorsh}{一般化運動量の具体例}
  \begin{exercise}
    デカルト座標$x$でラグランジアン$L$が
    \begin{equation*}
      L(x,\dot{x})=\half m\dot{x}^2 - U(x)
    \end{equation*}
    と与えられているとき、一般化運動量$p_x$を求めよ。これはニュートン力学で定義された運動量に一致するかどうかも答えよ。
  \end{exercise}
  \begin{exercise}
    二次元極座標$r,\theta$でラグランジアン$L$が
    \begin{equation*}
      L(r,\theta,\dot{r},\dot{\theta})=\half m (\dot{r}+r\dot{\theta})^2 -       U(r,\theta)
    \end{equation*}
と与えられているとき、一般化運動量$p_r,p_\theta$を求めよ。ただし添字はどの座標についての一般化運動量かを表している。
  \end{exercise}
  \begin{exercise}    上で求めた$p_\theta$について、これは解析力学の立場では角度座標$\theta$の一般化運動量であるが、ニュートン力学においてどのような量にあたるか答えよ。
  \end{exercise}
\end{majorsh}

\section{ラグランジアンの一意性？}
\label{sec:unique-lagrangian}

物理学で興味があるのは運動方程式であり、ラグランジアンや作用の値自身ではありません。ラグランジアンは運動方程式を導く為の情報を全て含んでいるので、
\begin{center}
  系 $\longleftrightarrow$ ラグランジアン
\end{center}
という対応関係があることは確かです。しかし、この系とラグランジアンの対応は、一対一対応でしょうか？　つまりここでは、ある系の運動方程式を与えるラグランジアンは、一意に定まるか？　と言う問題を考えてみます。答えから言えば、実は同じ運動方程式を与えるラグランジアンは無数に存在します。

\begin{majorsh}{ラグランジアンの非一意性}%% pure note:類体論的な考察をしたい
  ある系のラグランジアン$L(q,\dot{q},t)$が得られているとする。

  \begin{exercise}
    次の各場合について新しいラグランジアン$L'$が同じ運動方程式を導くことを示せ。
    \begin{minor}
    \item $L'=\lambda L \quad (\lambda \text{は任意の定数})$
    \item $L'=L + L_0  \quad (\L_0) \text{は任意の定数})$
    \item $L' = L + \diff{W}{t} \quad (Wは(q,t)\text{の任意の関数})$
    \end{minor}
  \end{exercise}

  \begin{exercise}
    前問のパターン以外にも、同じ運動方程式を導くラグランジアンが存在することを示せ。たとえば、
    $L=q^2 \dot{q}.  \diff{}{t} \pardiff{L}{\dot{q}} - \pardiff{L}{q} = 0  \rightarrow 2q\dot{q}  + -2q\dot{q}$
  \end{exercise}
\end{majorsh}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "theory-master"
%%% End: 
